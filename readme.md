# View the content of an elegantly JSON.

Source JSON: https://raw.githubusercontent.com/substack/node-browserify/master/package.json
[] - The user enters the URL of the JSON in a field and the application displays a table in HTML format with three columns.

[x] - The first column contains the key.

[x] - The second column provides an overview of the value as follows:
- String: Show the first 10 characters
- Array: Display size
- Object: View the number keys

[x] - The third column contains the type of the value (string, array, object).

constraints:
[x] - - Show only the first 10 keys in alphabetical order
[x] - - Only authorized jQuery Image

[x] - Apply a nice CSS table.

Duration: 1:30

# How to work with source

1 & npm install nodejs
2 execute npm install grunt -g
3 execute npm install bower -g
4 bower install npm install && && grunt watch
5 Have fun!